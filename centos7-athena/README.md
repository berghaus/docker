Athena-22.0.X Image Configuration (releases).
======================================

This configuration can be used to build an image providing a completely
standalone installation of an Athena-22.0.X release.

Building an image for an existing numbered release can be done with:

```bash
docker build -t <username>/athena:22.0.8 --build-arg PROJECT=Athena --build-arg RELEASE=22.0.8 .
```


Athena-22.0.X Image Configuration (nightlies)
======================================

This configuration can be used to build an image providing a completely
standalone installation of an Athena-22.0.X nightly.

Building an image for an existing nightly can be done with:

```bash
docker build -t <username>/athena:22.0.7_2019-10-09T2130 --build-arg RELEASE=22.0.7 --build-arg TIMESTAMP=2019-10-09T2130 .
```


Examples
--------

You can find some pre-built images of both types at:  
[atlas/athena](https://hub.docker.com/r/atlas/athena/)  
[atlas/athena/tags](https://hub.docker.com/r/atlas/athena/tags/) 

Additional Setup
======================================

The following environment variable needs to be exported manually, and include the local squid servers (if any)

FRONTIER_SERVER

For standalone mode (no cvmfs)

unset FRONTIER_SERVER
